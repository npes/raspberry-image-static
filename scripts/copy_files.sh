#!/usr/bin/env bash

echo "copying extra files"

MNTDIR="mnt"

cp templates/ifcfg-interface-eth0 $MNTDIR/etc/network/interfaces.d/eth0

echo adding Documents folder to pi user and copying scripts
mkdir $MNTDIR/home/pi/Documents
cp -v documents/test.py $MNTDIR/home/pi/Documents/test.py

git clone https://gitlab.com/21s-itt2-datacenter-students-group/examples/mqtt-tests.git
ls -la
cp -r -v mqtt-tests $MNTDIR/home/pi/Documents/mqtt-tests