#!/usr/bin/env bash

# exit if anything fails.
set -e


#DL_NAME="raspbian_lite_latest"
DL_NAME="raspios_lite_armhf_latest"
DL_URL="https://downloads.raspberrypi.org/$DL_NAME"

if [ "x" == "x$1" ]; then
  echo "usage $0 <workdir> <new image name>"
  exit 1
else
  WORKDIR=$1
  IMAGE_FILE=$2
fi

if [ -d $WORKDIR ]; then
	echo "$WORKDIR exist - assuming cache is ok"
	echo "- delete it to redownload"
	exit 0
fi

BINDIR=$(dirname $(readlink -f $0))
mkdir $WORKDIR
cd $WORKDIR

echo "Downloading $DL_NAME from $DL_URL (if updated)"
wget -nv -N $DL_URL

#echo hard linking
#if [ -f $IMAGE_ZIP ]; then rm $IMAGE_ZIP; fi
#ln $DL_NAME $IMAGE_ZIP

IMAGE=$(unzip -l $DL_NAME | grep .img | cut -d ' ' -f 7)
if [ -f $IMAGE ]; then rm $IMAGE; fi
echo "- Image file is $IMAGE"
unzip $DL_NAME

IMAGE=$(unzip -l $DL_NAME | grep .img | cut -d ' ' -f 7)
echo "- renaming $IMAGE to $IMAGE_FILE"
mv $IMAGE $IMAGE_FILE
